/*
  * The MIT License
  *
  * Copyright 2015 Eziama Ubachukwu (eziama.ubachukwu@gmail.com).
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */
/*
* File:   Heapsort.h
* Author: eziama ubachukwu
*
* Created on June 8, 2015, 9:06 AM
*/

#pragma once
#include "stdafx.h"
#include "global.h"
#include "Algorithm.h"

namespace inf2b
{
    class Heapsort {
    private:
        long cursor = 0;
        InputVectorType& input;

         void buildHeap() {
            // start from the rightmost h-1 level node
            std::cout << "[UPDATE]\tBulding heap..." << std::endl;
            std::cout << "[TASKRUNNER] Bulding heap..." << std::endl;
            int height = log2(input.size());
            std::cout << "[TREEHEIGHT]\t" << height << std::endl;
            long startNodeIndex = (int) pow(2.0, height) - 2;
            do {
                // heapify this subtree
                heapify(startNodeIndex);
            } while (--startNodeIndex >= 0);
        }

         void hsort() {
            std::cout << "[UPDATE]\tSorting..." << std::endl;
            std::cout << "[TASKRUNNER] Sorting..." << std::endl;
            while (cursor < (input.size() - 1)) {
                // exchange first and last elements
                swap(0, input.size() - cursor - 1);
                ++cursor;
                heapify(0);
            }
            int prev = 0;
            for (auto n : input) {
                assert(prev <= n);
                prev = n;
            }
        }

         void heapify(long nodeIndex) {
            long maxValue = nodeIndex;
            long left = getLeftChild(nodeIndex);
            long right = getRightChild(nodeIndex);

            if (hasLeftChild(nodeIndex) && input[left] > input[nodeIndex]) {
                maxValue = left;
            }
            if (hasRightChild(nodeIndex) && input[right] > input[maxValue]) {
                maxValue = right;
            }
            if (nodeIndex != maxValue) {
                swap(nodeIndex, maxValue);
                heapify(maxValue);
            }
        }

         bool hasLeftChild(long nodeIndex) {
            return (getLeftChild(nodeIndex) < (input.size() - cursor));
        }

         bool hasRightChild(long nodeIndex) {
            return (getRightChild(nodeIndex) < (input.size() - cursor));
        }

         long getLeftChild(long index) {
            // multiply by 2
            return ((index << 1) + 1);
        }

         long getRightChild(long index) {
            return ((index << 1) + 2);
        }

         long getParent(long index) {
            return ((index - 1) >> 1);
        }

         void swap(long indexX, long indexY) {
            auto temp = input[indexX];
            input[indexX] = input[indexY];
            input[indexY] = temp;
        }

    public:

        Heapsort(InputVectorType& v) : input (v){};

        ~Heapsort() { };

        void operator()() {
            buildHeap();
            hsort();
        };
    };
}