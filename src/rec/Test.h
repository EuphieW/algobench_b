/* 
 * File:   Test.h
 * Author: eziama
 *
 * Created on June 6, 2015, 8:04 AM
 */

#include <stddef.h>

#ifndef TEST_H
#define	TEST_H

template<class T>
class Test {
private:
    T* input;
    size_t size_;

public:
    Test();
    Test(size_t);
    Test(const Test& orig);
    virtual ~Test();
    void sayHello(T*);
};

#endif	/* TEST_H */

