/*
  * The MIT License
  *
  * Copyright 2015 Eziama Ubachukwu (eziama.ubachukwu@gmail.com).
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */
#pragma once
#include "stdafx.h"
#include "global.h"

namespace inf2b
{
    class Vertex;

    typedef uint32_t InputIntType;
    typedef std::vector<InputIntType> Row;
    typedef std::vector<Row> Matrix;
    typedef std::vector<Vertex> VertexList;
    typedef std::vector<Vertex*> VertexPtrList;

    struct VertexColour {
        int white = 0;
        int gray = 1;
        int black = 2;
    } Colour;

    class Vertex {
    private:
        const size_t value;
        int state;
        VertexPtrList adjPtrList;
        bool isDelayed;

        void addDelay() {
            // add an infinitesimally small pause, since graph generation duration
            // swamps execution time and even with large graphs (> 1,000,000 vertices),
            // search is still in the ms range while the former is in seconds
            std::this_thread::sleep_for(std::chrono::microseconds(1)); // unreliable
        };

    public:
        // isDelayed parameter determines if a delay should be added to every vertex and edge
        // visit to make the graph algos run slower and get more reasonable runtimes. The user
        // sends this param from the frontend
        Vertex(size_t value, bool isDelayed) : value {value}, isDelayed {isDelayed}, state {Colour.white} { };
        // copy cstor
        Vertex(const Vertex& v) : value {v.value} {
            std::cout << "Copy cstor" << std::endl;
            adjPtrList = v.adjPtrList;
        };
        // move cstor
        Vertex(const Vertex&& v) : value {std::move(v.value)} {
            std::cout << "Move cstor" << std::endl;
            adjPtrList = std::move(v.adjPtrList);
        };
        // copy assignment
        // TODO: correct these functions or delete 'em
        Vertex& operator=(Vertex& v) {
            std::cout << "Copy ass." << std::endl;
            *this = v;
            return *this;
        };
        // move assignment
        Vertex& operator=(Vertex&& v) {
            std::cout << "Move ass." << std::endl;
            return v;
        };
        // using smart pointers here would complicate matters unnecessarily: cycles will
        // be created with shared_ptr, necessitating introducing weak-ptr somewhere, and 
        // then converting the latter to shared_ptr to access the pointed-to object
        bool addNeighbour(Vertex* pV) {
            // make sure the neighbour hasn't been added before
            if (std::find_if(adjPtrList.begin(), adjPtrList.end(), [&pV] (const Vertex* p) { return pV == p; })
                == adjPtrList.end()) {
                //std::cout << "pVertex value: " << pV->value << " (Mem loc: " << pV << ")\n";
                adjPtrList.push_back(std::move(pV));
                return true;
            }
            return false;
        }

        const size_t getValue() {
            return value;
        }

        int getState() {
            return state;
        }
        /**
        This method simulates a slightly "heavier" vertex visit if isDelayed is true. This is included because
        the algo runs so fast that it takes too long to generate the structure, compared to running
        DFS or BFS.
        */
        void setState(int s) {
            if (isDelayed) {
                addDelay();
            }
            state = s;
        }

        void resetNeighbours() {
            adjPtrList.resize(0);

            // using vector::clear() will NOT reset capacity, only size,
            // which might be a good thing if we still will allocate more
        }

        VertexPtrList& getNeighbours() {
            // this is an edge visit - make it "heavier" if requested by user
            if (isDelayed) {
                addDelay();
            }
            return adjPtrList;
        }
    };

    class Graph {
    private:
        VertexList vertices;
        size_t numVertices;
        // set if a "more expensive" search should be used - see Vertex::setState()
        // for more details
        bool isDelayed;

    public:
        // no-params cstor
        Graph(bool isDelayed) : numVertices { }, isDelayed {isDelayed} { };
        // copy cstor
        Graph(const Graph& graph) { };
        // dstor
        ~Graph() { };
        // copy assignment
        // TODO: review
        Graph& operator=(const Graph& rhs) {
            numVertices = rhs.numVertices;
            vertices.reserve(numVertices);
            for (auto v : rhs.vertices)
                vertices.emplace_back(v);
            // change pointers to point to new graph vertices
            for (auto& v : vertices) {
                for (auto i = 0; i < v.getNeighbours().size(); ++i) {
                    v.getNeighbours()[i] = &vertices[v.getNeighbours()[i]->getValue()];
                }
            }
            return *this;
        };

        // move assignment
        // TODO: review
        Graph& operator=(Graph&& rhs) {
            vertices = std::move(rhs.vertices);
            numVertices = rhs.numVertices;
            return *this;
        };

        void addVertex(size_t key) {
            if (key >= numVertices) {
                throw std::string {"Assigning values beyond graph size."};
            }
            vertices.emplace_back(key, isDelayed);
        };

        VertexList& getVertices() {
            return vertices;
        };

        // array operator
        Vertex& operator[](size_t index) {
            return vertices[index];
        };

        // reserves space of newSize in vector, sets size, and returns former size
        size_t setNumVertices(size_t newSize) {
            vertices.reserve(newSize);
            auto tmp = numVertices;
            numVertices = newSize;
            return tmp;
        }

        size_t getNumVertices() {
            return numVertices;
        }

        void resetEdges() {
            for (auto& v : vertices) {
                v.resetNeighbours();
            }
        }

        void printVertices() {
            for (auto& v : vertices) {
                std::cout << "V: " << v.getValue() << "; S: " << v.getState()
                    << " M: " << &v << std::endl;
            }
        }

        void printGraph() {
            for (auto& v : vertices) {
                std::cout << "Vertex " << v.getValue() << "\tState: " << v.getState()
                    << "\tNeigbours: ";
                for (auto& n : v.getNeighbours()) {
                    std::cout << n->getValue() << ", ";
                }
                std::cout << std::endl;
            }
        }
    };

}
